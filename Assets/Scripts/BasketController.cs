using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.Interaction.Toolkit;

public class BasketController : MonoBehaviour
{
    [SerializeField] private XRGrabInteractable arrowPrefab;
    private XRSimpleInteractable interactable;
    private XRInteractionManager interactManager;

    private void Awake()
    {
        interactable = GetComponent<XRSimpleInteractable>();
        interactable.selectEntered.AddListener(GrabArrow);
        interactManager = FindObjectOfType<XRInteractionManager>();
    }

    private void GrabArrow(SelectEnterEventArgs arg0)
    {
        var arrow = Instantiate(arrowPrefab, Vector3.zero, Quaternion.identity);
        interactManager.ForceSelect(arg0.interactor, arrow);
    }
}
