using UnityEngine;

public class MovingTarget : Target
{

    void Update()
    {
        float minDx = Mathf.Min(Mathf.Abs(transform.position.x - leftDownFront.position.x),
            Mathf.Abs(transform.position.x - rightUpBack.position.x));
        float minDy = Mathf.Min(Mathf.Abs(transform.position.y - leftDownFront.position.y),
            Mathf.Abs(transform.position.y - rightUpBack.position.y));
        float minRadius = Mathf.Min(minDy, minDx);
        
    }
}
