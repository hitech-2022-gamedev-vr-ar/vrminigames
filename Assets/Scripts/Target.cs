using TMPro;
using UnityEngine;

public class Target : MonoBehaviour
{
    [SerializeField] protected Transform leftDownFront, rightUpBack;
    [SerializeField] private TextMeshProUGUI scoreNumberText;
    [SerializeField] private int a;
    private void Awake()
    {
        SetRandomPosition();
    }
    private void OnCollisionEnter(Collision collision)
    {
            Touched();
    }

    private void Touched()
    {
        string textScore = scoreNumberText.text;
        int score = int.Parse(textScore) + a;
        scoreNumberText.text = score.ToString();
        SetRandomPosition();
    }

    private void SetRandomPosition()
    {
        Vector3 new_position = new Vector3(Random.Range(leftDownFront.position.x, rightUpBack.position.x),
            Random.Range(leftDownFront.position.y, rightUpBack.position.y),
            Random.Range(leftDownFront.position.z, rightUpBack.position.z));
        //transform.position = new_position;
        Debug.Log(transform.parent.position);
        transform.parent.position = new_position;
        Debug.Log(new_position);
    }
}
