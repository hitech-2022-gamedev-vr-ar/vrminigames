using TMPro;
using UnityEngine;

public class ResetButton : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreNumberText;

    public void Reset()
    {
        scoreNumberText.text = "0";}
}
