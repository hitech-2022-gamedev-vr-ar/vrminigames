using TMPro;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class Beaver : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreNumberText;
    [SerializeField] private Vector3 position1 = Vector3.zero;
    [SerializeField] private Vector3 position2 = Vector3.zero;
    [SerializeField] private Vector3 position3 = Vector3.zero;
    [SerializeField] private Vector3 position4 = Vector3.zero;
    [SerializeField] private GameObject beaverPrefab;
    private Vector3[] positions;
    private float timer = 0f;
    private int score = 0;
    private BoxCollider boxCollider;

    void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
        scoreNumberText.text = score.ToString();
        positions = new Vector3[4] { position1, position2, position3, position4 };
    }
    private void Update()
    {
        if (timer < Time.time)
        {
            SetRandomPosition();
            timer += Random.Range(0.1f, 1);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 7)
        {
            score += 1;
            scoreNumberText.text = score.ToString();
            timer = Time.time + Random.Range(0.1f, 1);
            beaverPrefab.SetActive(false);
            boxCollider.enabled = false;
        }
    }

    private void SetRandomPosition()
    {
        boxCollider.enabled = true;
        beaverPrefab.SetActive(true);
        int newPositionIndex = Random.Range(0, 3);
        transform.position = positions[newPositionIndex];
    }
}
