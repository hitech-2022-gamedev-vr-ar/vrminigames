using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class AttachArrow : MonoBehaviour
{
    private LineRenderer lineRenderer;
    private Transform anchor;
    private Vector3 position;

    private void Awake()
    {
        lineRenderer = GetComponentInChildren<LineRenderer>();
        position = lineRenderer.GetPosition(1);
    }

    /*  [SerializeField] private Vector3 arrowOffset = new Vector3(0f, 0f, -0.2f);
      [SerializeField] protected Vector3 arroRotation = Vector3.left;*/

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 6)
        { 
            anchor = other.GetComponent<XRGrabInteractable>().attachTransform;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == 6)
        {
            var point = anchor.position - transform.position;
            point.x = 0f;
            lineRenderer.SetPosition(1, point);
            /*other.transform.SetParent(transform);
            other.attachedRigidbody.isKinematic = true;
            other.transform.localPosition = arrowOffset;
            other.transform.localRotation = Quaternion.AngleAxis(-90, arroRotation);*/
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 6)
        {
            Vector3 delta = position - lineRenderer.GetPosition(1);
            if (delta.z > 0f)
            {
                float speed = delta.magnitude * 100;
                other.attachedRigidbody.AddForce(other.transform.forward * speed, ForceMode.Impulse);
            }
            lineRenderer.SetPosition(1, position);

        }
    }
}
